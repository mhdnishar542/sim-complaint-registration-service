/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.sim.complaint.registration.service.service;

import in.ac.gpckasaragod.sim.complaint.registration.service.model.ui.ComplaintUiModel;
import in.ac.gpckasaragod.sim.complaint.registration.service.model.ui.data.ComplaintDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface ComplaintDetailService {
    public String saveComplaintDetail(Integer complaintId, String mobNo, String simType);
    public ComplaintDetails readComplaintDetail(Integer Id);
    public List<ComplaintUiModel> getAllComplaintDetail();
    public String updateComplaintDetail(Integer id,String mobNo,Integer complaintId,String simType);
    public String deleteComplaintDetail(Integer id);

    
}
