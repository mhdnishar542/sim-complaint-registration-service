/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.sim.complaint.registration.service.service.impl;

import in.ac.gpckasaragod.sim.complaint.registration.service.model.ui.ComplaintUiModel;
import in.ac.gpckasaragod.sim.complaint.registration.service.model.ui.data.ComplaintDetails;
import in.ac.gpckasaragod.sim.complaint.registration.service.service.ComplaintDetailService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class ComplaintDetailServiceImpl extends ConnectionServiceImpl implements ComplaintDetailService{
    @Override
    public String saveComplaintDetail(Integer complaintId, String mobNo, String simType) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO COMPLAINT_DETAILS (MOB_NO,COMPLAINT_ID,SIM_TYPE) VALUES " 
                    + "('"+mobNo+ "',"+complaintId+ ",'"+simType+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
            }else{
                return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(ComplaintServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "Save failed";
        }
}
    @Override
    public ComplaintDetails readComplaintDetail(Integer Id){
        ComplaintDetails complaintDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM COMPLAINT_DETAILS WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                String mobNo = resultSet.getString("MOB_NO");
                Integer complaintId = resultSet.getInt("COMPLAINT_ID");
                String simType = resultSet.getString("SIM_TYPE");
                complaintDetails = new ComplaintDetails(id,complaintId,mobNo,simType);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ComplaintServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return complaintDetails;
       
    }
    @Override
     public List<ComplaintUiModel> getAllComplaintDetail(){
        List<ComplaintUiModel> complaintuimodel = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT COMPLAINT_DETAILS.ID,NETWORK_TYPE,COMPLAINT,REASON,MOB_NO,SIM_TYPE FROM COMPLAINT_DETAILS JOIN COMPLAINTS ON COMPLAINTS.ID=COMPLAINT_DETAILS.COMPLAINT_ID";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             Integer id = resultSet.getInt("ID");
             String networkType = resultSet.getString("NETWORK_TYPE");
             String description = resultSet.getString("COMPLAINT");
             String reason = resultSet.getString("REASON");
                String mobNo = resultSet.getString("MOB_NO");                
                String simType = resultSet.getString("SIM_TYPE");
                ComplaintUiModel complaintUiModel = new ComplaintUiModel(id,networkType,description,reason,mobNo,simType);
                complaintuimodel.add(complaintUiModel);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(ComplaintServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return complaintuimodel;
    
    }
     
    @Override
     public String updateComplaintDetail(Integer id,String mobNo,Integer complaintId,String simType){
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE COMPLAINT_DETAILS SET MOB_NO='"+mobNo+"',COMPLAINT_ID="+complaintId+",SIM_TYPE='"+simType+"' WHERE ID="+id;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(Exception ex){
            return "Update failed";
        }
    }
    @Override
     public String deleteComplaintDetail(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM COMPLAINT_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (Exception ex) {
           ex.printStackTrace();
           return "Delete failed";
        }
     
}

}


