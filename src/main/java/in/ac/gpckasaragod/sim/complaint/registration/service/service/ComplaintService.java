/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.sim.complaint.registration.service.service;

import in.ac.gpckasaragod.sim.complaint.registration.service.model.ui.data.Complaint;
import java.util.List;

/**
 *
 * @author student
 */
public interface ComplaintService {
        public String saveComplaint(String networkType,String complaint,String reason);
        public Complaint readComplaint(Integer Id);
        public List<Complaint> getAllComplaints();
        public String updateComplaint(Integer id,String networktype,String reason,String complaint);

    public String deleteComplaint(Integer id);
        
    }

