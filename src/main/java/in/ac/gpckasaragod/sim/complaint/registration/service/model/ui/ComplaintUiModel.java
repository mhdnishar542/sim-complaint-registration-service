/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.sim.complaint.registration.service.model.ui;

/**
 *
 * @author student
 */
public class ComplaintUiModel {
   private Integer id;
   private String networkType;
    private String complaint;
    private String reason;
    private String mobNo;
    private String simType;

    public ComplaintUiModel(Integer id, String networkType, String complaint, String reason, String mobNo, String simType) {
        this.id = id;
        this.networkType = networkType;
        this.complaint = complaint;
        this.reason = reason;
        this.mobNo = mobNo;
        this.simType = simType;
    }
    
   

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public String getComplaint() {
        return complaint;
    }

    public void setComplaint(String complaint) {
        this.complaint = complaint;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

    public String getSimType() {
        return simType;
    }

    public void setSimType(String simType) {
        this.simType = simType;
    }

    

    
    
}
