/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.sim.complaint.registration.service.model.ui.data;

/**
 *
 * @author student
 */
public class Complaint {

    private Integer id;
    private String networkType;
    private String complaint;
     private String reason;

    public Complaint(Integer id, String networkType, String complaint, String reason) {
        this.id = id;
        this.networkType = networkType;
        this.complaint = complaint;
        this.reason = reason;
    }
    @Override
    public String toString(){
        return networkType+"-"+complaint; 
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNetworkType() {
        return networkType;
    }

    public void setNetworkType(String networkType) {
        this.networkType = networkType;
    }

    public String getComplaint() {
        return complaint;
    }

    public void setComplaint(String complaint) {
        this.complaint = complaint;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    

}
