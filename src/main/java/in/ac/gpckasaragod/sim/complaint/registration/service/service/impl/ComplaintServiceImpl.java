/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.sim.complaint.registration.service.service.impl;

import in.ac.gpckasaragod.sim.complaint.registration.service.model.ui.data.Complaint;
import in.ac.gpckasaragod.sim.complaint.registration.service.service.ComplaintService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class ComplaintServiceImpl extends ConnectionServiceImpl implements ComplaintService{

    @Override
    public String saveComplaint(String networkType, String complaint,String reason) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO COMPLAINTS (NETWORK_TYPE,COMPLAINT,REASON) VALUES " 
                    + "('"+networkType+ "','" +complaint+ "','" +reason+"')" ;
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
            }else{
                return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(ComplaintServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "Save failed";
        }
    }
    @Override
    public Complaint readComplaint(Integer Id){
        Complaint complaint = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM COMPLAINTS WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                String networkType = resultSet.getString("NETWORK_TYPE");
                String description = resultSet.getString("COMPLAINT");
                String reason = resultSet.getString("REASON");
                complaint = new Complaint(id,networkType,description,reason);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(ComplaintServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return complaint;
       
    }
    @Override
    public List<Complaint> getAllComplaints(){
        List<Complaint> complaints = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM COMPLAINTS";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             int id = resultSet.getInt("ID");
                String networkType = resultSet.getString("NETWORK_TYPE");
                String description = resultSet.getString("COMPLAINT");
                String reason = resultSet.getString("REASON");
                Complaint complaint = new Complaint(id,networkType,description,reason);
                complaints.add(complaint);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(ComplaintServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return complaints;
    
    }
    
    @Override
    public String updateComplaint(Integer id,String networkType,String complaint,String reason){
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE COMPLAINTS SET NETWORK_TYPE='"+networkType+"',COMPLAINT='"+complaint+"',REASON='"+reason+"' WHERE ID="+id;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(Exception ex){
            return "Update failed";
        }
    }

    @Override
    public String deleteComplaint(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM COMPLAINTS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (Exception ex) {
           ex.printStackTrace();
           return "Delete failed";
        }
       
       
    }
    
                
}

                
                
                
                
                
                
    

        
        
