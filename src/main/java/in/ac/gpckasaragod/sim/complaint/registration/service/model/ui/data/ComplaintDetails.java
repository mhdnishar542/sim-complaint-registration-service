/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.sim.complaint.registration.service.model.ui.data;

/**
 *
 * @author student
 */
public class ComplaintDetails {
    private Integer id;
    private Integer complaintId;
    private String mobNo;
    private String simType;

    public ComplaintDetails(Integer id, Integer complaintId, String mobNo, String simType) {
        this.id = id;
        this.complaintId = complaintId;
        this.mobNo = mobNo;
        this.simType = simType;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getComplaintId() {
        return complaintId;
    }

    public void setComplaintId(Integer complaintId) {
        this.complaintId = complaintId;
    }

    public String getMobNo() {
        return mobNo;
    }

    public void setMobNo(String mobNo) {
        this.mobNo = mobNo;
    }

    public String getSimType() {
        return simType;
    }

    public void setSimType(String simType) {
        this.simType = simType;
    }
    
}